import * as fs from 'fs';

export function countWords(file: string): Promise<number> {

    return new Promise((resolve, reject) => {

        fs.readFile(file, 'utf8', (err, data) => {
        if (err) {
            reject(err);
            return;
        }

        const wordCount = data.split(/\s+/).filter(word => word !== '').length;
        resolve(wordCount);
        });
    });
}
