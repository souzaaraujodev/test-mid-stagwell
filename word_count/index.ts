import * as path from 'path';
import { countWords } from './count-words.helper';

async function main() {
    try {
        const filePath = path.join(__dirname, 'lorem-ipsum.txt');
        const wordCount = await countWords(filePath);
        console.log(`Word count: ${wordCount}`);
    } catch (err) {
        console.error('Error reading the file');
    }
}

main();