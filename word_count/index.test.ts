import {describe, expect, test} from '@jest/globals';
import fs from 'fs';
import path from 'path';
import { countWords } from './count-words.helper';


describe('Word Count exercise', () => {
  let tempFile: string;
  beforeAll(() => {
    const tempDir = fs.mkdtempSync(path.join('/tmp/', 'sample-'));
    tempFile = path.join(tempDir, 'words.txt');
    const content = 'Lorem Ipsum is simply dummy text of\nthe printing and typesetting industry.';
    fs.writeFileSync(tempFile, content);
  });

  test('Word count', async () => {
    const wordCount = await countWords(tempFile);
    expect(wordCount).toBe(12);
  });

  test('Incorrect file to open', async () => {
    expect(countWords('path-incorrect')).rejects.toThrow();
  });

});