describe('My Contact Form', () => {
  it('Fill all fields and return success', () => {
    cy.visit('../contact_form/index.html');
    cy.get('input[name="name"]').type('John Doe');
    cy.get('input[name="email"]').type('john@example.com');
    cy.get('input[name="subject"]').type('Hello World');
    cy.get('textarea[name="message"]').type('This is a test message from website contact form.');

    cy.get('button[type="submit"]').click();
    cy.contains('Form submitted successfully');
  });

  it('Retrieve fail for empty name field', () => {
    cy.visit('../contact_form/index.html');

    cy.get('input[name="email"]').type('john@example.com');
    cy.get('input[name="subject"]').type('Hello World');
    cy.get('textarea[name="message"]').type('This is a test message from website contact form.');
    
    cy.get('button[type="submit"]').click();
    cy.contains('Name field is required');
  });

  it('Retrieve fail for empty email field', () => {
    cy.visit('../contact_form/index.html');

    cy.get('input[name="name"]').type('John Doe');
    cy.get('input[name="subject"]').type('Hello World');
    cy.get('textarea[name="message"]').type('This is a test message from website contact form.');
    
    cy.get('button[type="submit"]').click();
    cy.contains('Email field is required');
    cy.contains('Email field is invalid');
  });

  it('Retrieve fail for short subject field', () => {
    cy.visit('../contact_form/index.html');

    cy.get('input[name="name"]').type('John Doe');
    cy.get('input[name="email"]').type('john@example.com');
    cy.get('input[name="subject"]').type('Hell');
    cy.get('textarea[name="message"]').type('This is a test message from website contact form.');
    
    cy.get('button[type="submit"]').click();
    cy.contains('Subject field must be at least 5 characters');
  });

  it('Retrieve fail for short message field', () => {
    cy.visit('../contact_form/index.html');

    cy.get('input[name="name"]').type('John Doe');
    cy.get('input[name="email"]').type('john@example.com');
    cy.get('input[name="subject"]').type('Hello World');
    cy.get('textarea[name="message"]').type('This is...');
    
    cy.get('button[type="submit"]').click();
    cy.contains('Message field must be at least 15 characters');
  });
});
