export class Stack {
    private _items: string[] = [];

    push(item: string): void {
        this._items.push(item);
    }

    pop(): string | undefined {
        return this._items.pop();
    }

    isEmpty(): boolean {
        return this._items.length === 0;
    }
}

const todoStack = new Stack();
todoStack.push("Wake Up");
todoStack.push("Prepare coffee");
todoStack.push("Codding");

console.log(todoStack.pop());
console.log(todoStack.pop());
console.log(todoStack.pop());
console.log(todoStack.isEmpty());