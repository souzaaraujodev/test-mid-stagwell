import {describe, expect, test} from '@jest/globals';
import { Stack } from './index';

describe('Stack exercise', () => {

    let stack: Stack;

    beforeAll(() => {
      stack = new Stack();
    });
  
    test('Push a new item', () => {
      expect(() => stack.push('Take out the trash')).not.toThrow();
    });

    test('Pop item', () => {
      expect(stack.pop()).toBe('Take out the trash');
    });

    test('Check is Empty', () => {
      expect(stack.isEmpty()).toBeTruthy();
    });
  });