import {isPalindrome} from './index';
import {describe, expect, test} from '@jest/globals';

describe('Palindrome exercise', () => {
  
    test('Palindrome with One Word (short)', () => {
      expect(isPalindrome('ana')).toBeTruthy();
    });

    test('Palindrome with One Word (long)', () => {
      expect(isPalindrome('aibofobia')).toBeTruthy();
    });

    test('Palindrome phrase', () => {
      expect(isPalindrome('Anotaram a data da maratona')).toBeTruthy();
    });

    test('Is not palindrome - word', () => {
      expect(isPalindrome('leandro')).toBeFalsy();
    });

    test('Is not palindrome - phrase', () => {
      expect(isPalindrome('O rato roeu a roupa do rei de roma')).toBeFalsy();
    });
  });