/**
 * Check if a string is a palindrome
 * @date 7/24/2023 - 2:48:14 PM
 *
 * @param {string} str
 * @returns {boolean}
 */
export const isPalindrome = (str: string): boolean => {
    const cleanned = str.replace(/\s+/g, '').toLowerCase();
    return cleanned === cleanned
        .split('')
        .reverse()
        .join('');
}

console.log(`'Ame o poema' is isPalindrome? ${isPalindrome('Ame o poema')}`);