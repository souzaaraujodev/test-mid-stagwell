# Test Mid Repository

Leandro de Souza Araujo
leandro.souara.web@gmail.com
+55 67 99160-4334

## Run Jest Tests

To run execute: `npm i` and `npm run test`

## Folder: Factorial

Algorithm exercise - Factorial:
Write a function that takes a positive integer as input and returns the factorial of that number.
For example, if the given number is 5, the function should return 120 (1 _ 2 _ 3 _ 4 _ 5).

- Run without Jest: `npm run factorial`

## Folder: Palindrome

Write a function that checks whether a word or phrase is a palindrome. A palindrome is a
word that is read the same from left to right as from right to left, ignoring spaces and
differentiating between case and emergency letters.

- Run without Jest: `npm run palindrome`

## Folder: Contact Form

Create a simple web page containing a contact form with fields for name, email, subject and
message. When submitting the form, the data must be validated (for example, checking that
the email field has a valid format) and displayed in an area below the form.

- Run oppening on browser the file: `contact_form/index.html`
  or use cypress test with `npm run test:cypress`

## Folder: Stack

Implement a stack data structure in a programming language of your choice. Then write
functions to push (push) an element, pop (pop) an element, and check if the stack is empty.

- Run without Jest: `npm run stack`

## Folder: Word Count

Write a program that counts the number of words in a text file given as input. The program
must disregard whitespace and evaluation.

- Run without Jest: `npm run word-count`

## Folder: Fibonacci

Write a function that calculates the nth number in the Fibonacci sequence. The Fibonacci
sequence is formed by adding the two previous numbers, starting with 0 and 1 (or 1 and 1,
depending on the initial order), and the first numbers in the sequence are: 0, 1, 1, 2, 3, 5, 8, 13, ...

- Run without Jest: `npm run fibonacci`
