/**
 * Returns the factorial of a number.
 * @date 7/24/2023 - 1:47:48 PM
 *
 * @param {number} number
 * @returns {number}
 */
export function factorial(number: number): number {
    const mustBeResultOne = (n: number) => n === 0 || n === 1;
    const isNumberNegative = (n: number) => n < 0;

    if(isNumberNegative(number)) throw new Error("Only positive numbers are allowed.");
    if(mustBeResultOne(number)) return 1;
    
    let sum = 1;
    for (let i = 2; i <= number; i++) {
        sum *= i;
    }
    
    return sum;
}

console.log(`Factorial of 5 is ${factorial(5)}`);