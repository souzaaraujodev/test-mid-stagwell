import {factorial} from './index';
import {describe, expect, test} from '@jest/globals';

describe('Factorial exercise', () => {
  
    test('Factorial of 0', () => {
      expect(factorial(0)).toBe(1);
    });

    test('Factorial of 1', () => {
      expect(factorial(1)).toBe(1);
    });

    test('Factorial of 5', () => {
      expect(factorial(5)).toBe(120);
    });

    test('Factorial of 10', () => {
      expect(factorial(10)).toBe(3628800);
    });

    test('Factorial of negative number', () => {
      expect(() => {
        factorial(-5);
      }).toThrow('Only positive numbers are allowed.');
    });
  });