import {describe, expect, test} from '@jest/globals';
import { fibonacci } from './index';

describe('Fibonacci exercise', () => {
  
    test('Fibonacci number 1', () => {
      expect(fibonacci(1)).toBe(0);
    });

    test('Fibonacci number 2', () => {
      expect(fibonacci(2)).toBe(1);
    });

    test('Fibonacci number 5', () => {
      expect(fibonacci(5)).toBe(3);
    });

    test('Fibonacci number 8', () => {
      expect(fibonacci(8)).toBe(13);
    });

    test('Fibonacci of negative number', () => {
      expect(() => {
        fibonacci(-5);
      }).toThrow('Number must be a positive integer.');
    });
  });