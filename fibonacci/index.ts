
/**
 * Fibonacci
 * @date 7/24/2023 - 11:24:42 PM
 *
 * @export
 * @param {number} n
 * @returns {number}
 */
export function fibonacci(n: number): number {
    const isNumberNegativeOrZero = (n: number) => n <= 0;
    if(isNumberNegativeOrZero(n)) throw new Error("Number must be a positive integer.");

    if(n === 1) return 0;
    if(n === 2) return 1;

    let prevPrevNum = 0;
    let prevNum = 1;
    let currentNum = 0;
  
    for (let i = 3; i <= n; i++) {
      currentNum = prevPrevNum + prevNum;
      prevPrevNum = prevNum;
      prevNum = currentNum;
    }
  
    return currentNum;
  }

  console.log(`Fibonacci of 8 is: ${fibonacci(8)}`)